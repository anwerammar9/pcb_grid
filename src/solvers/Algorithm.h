#pragma once
#include "../grid_engine/Location.h"
#include <vector>
#include "../grid_engine/Grid.h"
//base class for all algorithms. useful for factory pattern.
class Algorithm {
public:
	virtual void SolveAlgorithm(const Location& srcpos, const Location& targetpos, const std::vector<Location>& obstacles, Grid &grid, sf::RenderWindow& createwindow) = 0;
	virtual void constructPath(Grid& grid) = 0;
};